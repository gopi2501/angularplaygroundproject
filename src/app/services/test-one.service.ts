import { Injectable, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/operators';
import { map } from "rxjs/operators";
// import { HttpClient } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class TestOneService implements OnInit {

  private count: number;

  constructor(private http: Http) {
   }

  ngOnInit(){
    // for(var i =0; i<3; i ++) {
      // this.makeACall();
    // }
  }

  makeACall(){
    console.log('making call');
    // this.http.get('https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo').
    // map((res) => { console.log(res.json()); });
    // this.count ++;
    this.http.get("http://jsonplaceholder.typicode.com/users")
    .pipe(map(res => res.json())).subscribe();
      // map((response) ⇒ response.json()).
      // subscribe((data) ⇒ console.log(data))
  }
}
