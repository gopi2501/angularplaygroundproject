import { TestBed } from '@angular/core/testing';

import { TestOneService } from './test-one.service';

describe('TestOneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TestOneService = TestBed.get(TestOneService);
    expect(service).toBeTruthy();
  });
});
