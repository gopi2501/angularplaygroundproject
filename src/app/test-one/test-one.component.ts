import { Component, OnInit } from '@angular/core';
import { TestOneService } from '../services/test-one.service';

@Component({
  selector: 'app-test-one',
  templateUrl: './test-one.component.html',
  styleUrls: ['./test-one.component.scss']
})
export class TestOneComponent implements OnInit {


  private childObjects: Object = [
    { value: 'USA'},
    { value: 'SRI LANKA'},
    { value: 'UK'},
    { value: 'INDIA', children: [
      {value: 'hyderabad'},
      {value: 'delhi'},
      {value: 'mumbai'},

    ]}
  ];

  constructor(private testOneService: TestOneService) { }

  ngOnInit() {
  }

  callService(){
    for(var i =0; i<3; i ++) {
      console.log('making call to service');
    this.testOneService.makeACall();
  }
}

}
