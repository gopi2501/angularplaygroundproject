import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestChildOnComponent } from './test-child-on.component';

describe('TestChildOnComponent', () => {
  let component: TestChildOnComponent;
  let fixture: ComponentFixture<TestChildOnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestChildOnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestChildOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
