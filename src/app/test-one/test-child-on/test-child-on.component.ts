import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-test-child-on',
  templateUrl: './test-child-on.component.html',
  styleUrls: ['./test-child-on.component.scss']
})
export class TestChildOnComponent implements OnInit, AfterViewInit {

  @Input('childObjects') childObjects: Object;

  @ViewChild(TestChildOnComponent) childCities: TestChildOnComponent;
  @ViewChild('spanObject') spanObject: ElementRef;
  constructor() {
  }

  print() {
    console.log(this.childCities);
  }
  ngOnInit() {
  }
  public ngAfterViewInit(){
    if(this.childCities){
    console.log(this.childCities.spanObject);
    // this.childCities.spanObject.nativeElement.display = 'none';
    this.childCities.spanObject.nativeElement.hidden = true;
    // this.displayingTheBanner.nativeElement.display = 'none';
  }}
}
