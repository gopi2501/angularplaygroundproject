import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TestOneComponent } from './test-one/test-one.component';
import { TestChildOnComponent } from './test-one/test-child-on/test-child-on.component';

import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    TestOneComponent,
    TestChildOnComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
